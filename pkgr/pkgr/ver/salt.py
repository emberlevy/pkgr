# Import python libs
import os
import shutil
import subprocess


def gather(hub):
    """
    Gather the version number from salt
    """
    pybin = hub.OPT.pkgr.python
    cdir = os.path.join(hub.pkgr.SDIR, "salt")
    cwd = os.getcwd()
    os.chdir(cdir)
    subprocess.run(f"{pybin} setup.py sdist", shell=True)
    for fn in os.listdir("dist"):
        ret = fn[fn.index("-") + 1 : fn[: fn.rindex(".")].rindex(".")].replace("-", "_")
    subprocess.run(f"git clean -fdx", shell=True)
    subprocess.run(f"git reset --hard HEAD", shell=True)
    os.chdir(cwd)
    return ret
